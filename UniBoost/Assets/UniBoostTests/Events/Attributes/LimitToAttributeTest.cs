using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UniBoost.Sys;

namespace UniBoost.Test.Sys
{
    [TestFixture]
    public class LimitToAttributeTest
    {
        [Test]
        public void TestAttributeList()
        {
            LimitToAttribute limit = new LimitToAttribute(typeof(TestFixtureAttribute));

            Assert.That(limit.ValidTypes, Has.Member(typeof(LimitToAttributeTest)));
        }
       
    }
}
