using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UniBoost.Sys;

namespace UniBoost.Test.Sys
{
    [TestFixture]
    public class LimitToTypeListTest
    {
        private class ClassA
        {
        }

        private class ClassB
        {
        }

        private class ClassC : ClassA
        {
        }

        private class ClassD : ClassA
        {
        }

        private class ClassE : ClassD
        {
        }

        [Test]
        public void TestSpecifiedTypeList()
        {
            LimitToTypeList typeList = new LimitToTypeList(LimitToTypeListOptions.SpecifiedOnly, typeof(ClassA), typeof(ClassD));
            Assert.That(typeList.ValidTypes, Is.EquivalentTo(new System.Type[] { typeof(ClassA), typeof(ClassD) } ));
        }

        [Test]
        public void TestDerivedTypeList()
        {
            LimitToTypeList typeList = new LimitToTypeList(LimitToTypeListOptions.SpecifiedAndDerived, typeof(ClassA));
            Assert.That(typeList.ValidTypes, Is.EquivalentTo(new System.Type[] { typeof(ClassA), typeof(ClassC), typeof(ClassD), typeof(ClassE) }));
        }
    }

}