using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UniBoost.Events;

namespace UniBoost.Test.Events
{
    [TestFixture]
    public class EventRouterTest
    {
        public enum FirstEventType
        {
            EventA,
            EventB
        }

        public enum SecondEventType
        {
            EventC,
            EventD
        }

        [SetUp]
        public void SetUp()
        {
            firstCounter = 0;
            secondCounter = 0;
            allCounter = 0;

            m_router = new EventRouter();
        }

        [Test]
        public void TestEnums()
        {
            // Test single and all routing
            m_router.Subscribe(typeof(FirstEventType), IncrementFirst);
            m_router.Subscribe(typeof(SecondEventType), IncrementSecond);
            m_router.Subscribe(IncrementAll);
            m_router.Publish(FirstEventType.EventA);
            Assert.That(firstCounter, Is.EqualTo(1));
            Assert.That(secondCounter, Is.EqualTo(0));
            Assert.That(allCounter, Is.EqualTo(1));


            // Test alternate single
            m_router.Publish(SecondEventType.EventC);
            Assert.That(firstCounter, Is.EqualTo(1));
            Assert.That(secondCounter, Is.EqualTo(1));
            Assert.That(allCounter, Is.EqualTo(2));


            // Test second single
            m_router.Subscribe(typeof(FirstEventType), IncrementFirstExtra);
            m_router.Publish(FirstEventType.EventA);
            Assert.That(firstCounter, Is.EqualTo(4));
            Assert.That(secondCounter, Is.EqualTo(1));
            Assert.That(allCounter, Is.EqualTo(3));


            // Check removing the delegates work
            m_router.Unsubscribe(typeof(FirstEventType), IncrementFirst);
            m_router.Unsubscribe(IncrementAll);
            m_router.Publish(FirstEventType.EventA);
            Assert.That(firstCounter, Is.EqualTo(6));
            Assert.That(secondCounter, Is.EqualTo(1));
            Assert.That(allCounter, Is.EqualTo(3));

        }

        private void IncrementFirst(System.Enum type, GameObject obj, params object[] parameters)
        {
            firstCounter++;
        }

        private void IncrementFirstExtra(System.Enum type, GameObject obj, params object[] parameters)
        {
            firstCounter += 2;
        }

        private void IncrementSecond(System.Enum type, GameObject obj, params object[] parameters)
        {
            secondCounter++;
        }

        private void IncrementAll(System.Enum type, GameObject obj, params object[] parameters)
        {
            allCounter++;
        }

        private int firstCounter;
        private int secondCounter;
        private int allCounter;

        EventRouter m_router;
    }
}
