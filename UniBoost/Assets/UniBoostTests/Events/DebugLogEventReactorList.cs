using UnityEngine;
using System.Collections.Generic;


namespace UniBoost.Events
{
    public class DebugLogEventReactorList : MonoBehaviour, IEventReactionList
    {
        [System.Serializable]
        public class DebugLogReaction : EventReaction
        {
            public override void React(GameObject origin, params object[] parameters)
            {
                Debug.Log(string.Format("{0} received from {1}", Trigger, origin.name));
            }
        }

        public IEnumerable<EventReaction> Reactions
        {
            get
            {
                return m_reactions;
            }
        }

        public void SetReactionArray(DebugLogReaction[] reactions)
        {
            m_reactions = reactions;
        }

        [SerializeField]
        private DebugLogReaction[] m_reactions;
    }
}
