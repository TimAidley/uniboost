using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UniBoost.Events;
using UniBoost.Sys;

namespace UniBoost.Test.Events
{
    [TestFixture]
    public class EventReactorTest
    {
        [Test]
        public void TestReaction()
        {
            GameObject gObj = new GameObject();
            EventReactor reactor = gObj.AddComponent<EventReactor>();
            DebugLogEventReactorList list = gObj.AddComponent<DebugLogEventReactorList>();

            DebugLogEventReactorList.DebugLogReaction[] reactions = new DebugLogEventReactorList.DebugLogReaction[1];
            reactor.TriggerType = typeof(TextureFormat);

            reactions[0] = new DebugLogEventReactorList.DebugLogReaction()
            {
                Trigger = TextureFormat.RGBA32
            };
            list.SetReactionArray(reactions);

            reactor.OnEnable();
        }
        
    }
}
