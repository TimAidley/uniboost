using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UniBoost.Sys;


namespace UniBoostTests.Sys
{
	[TestFixture]
	public class SerializableTypeTest
	{

        [Test]
        public void TestCreation()
        {
            SerializableType type = new SerializableType();
            type.Type = typeof(SerializableTypeTest);

            Assert.AreEqual(type.Type, typeof(SerializableTypeTest));
        }

        [Test]
        [ExpectedException(typeof(SerializableType.TypeUnsetException))]
        public void UninitialisedTest()
        {
            SerializableType type = new SerializableType();
#pragma warning disable 0219
            System.Type returnedType = type.Type;
#pragma warning restore 0219
        }

        // It'd be nice to test the serialisation here, but not sure Unity will
        // allow it.

	}
}
