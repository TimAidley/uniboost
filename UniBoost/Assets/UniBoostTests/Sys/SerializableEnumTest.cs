using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UniBoost.Sys;

namespace UniBoost.Test.Sys
{
    [TestFixture]
    public class SerializableEnumTest
    {
        private enum TestEnum
        {
            TestValue1,
            TestValue2
        }

        [Test]
        public void TestCreation()
        {
            SerializableEnum newEnum = new SerializableEnum();
            newEnum.Enum = TestEnum.TestValue1;

            Assert.AreEqual((TestEnum)newEnum.Enum, TestEnum.TestValue1);
            Assert.AreEqual(typeof(TestEnum), newEnum.Type);
        }
    }
}
