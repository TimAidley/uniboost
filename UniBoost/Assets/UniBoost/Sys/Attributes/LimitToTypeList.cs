using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace UniBoost.Sys
{
    public enum LimitToTypeListOptions
    {
        SpecifiedOnly,
        SpecifiedAndDerived
    }

    public class LimitToTypeList : PropertyAttribute
    {

        

        public LimitToTypeList(LimitToTypeListOptions options, params System.Type[] validTypes)
        {
            switch (options)
            {
                case LimitToTypeListOptions.SpecifiedOnly:
                    m_typeList = validTypes;
                    break;
                case LimitToTypeListOptions.SpecifiedAndDerived:
                    var possibleTypes = from assembly in System.AppDomain.CurrentDomain.GetAssemblies()
                                        from type in assembly.GetTypes()
                                        select type;
                    var matchTypes = from baseType in validTypes
                                     from possType in possibleTypes
                                     where baseType.IsAssignableFrom(possType)
                                     select possType;
                    m_typeList = matchTypes.ToArray();
                    break;
            }
        }

        public IList<System.Type> ValidTypes
        {
            get
            {
                return m_typeList;
            }
        }

        private System.Type[] m_typeList;
    }
}
