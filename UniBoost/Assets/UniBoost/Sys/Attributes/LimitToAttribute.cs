using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace UniBoost.Sys
{
    public class LimitToAttribute : PropertyAttribute
    {

        public LimitToAttribute(System.Type attribute)
        {
            m_typeList = (from assembly in System.AppDomain.CurrentDomain.GetAssemblies()
                          from type in assembly.GetTypes()
                          from attr in type.GetCustomAttributes(true)
                          where attr.GetType().Equals(attribute)
                          select type).ToArray();
        }

        public IList<System.Type> ValidTypes
        {
            get
            {
                return m_typeList;
            }
        }

        private System.Type[] m_typeList;
    }
}
