//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;

namespace UniBoost.Sys
{
    /// <summary>
    /// A serializable wrapper for System.Type. 
    /// Type is held as a string for the purposes of serialisation.
    /// </summary>
    [System.Serializable]
    public class SerializableType
    {
        public SerializableType()
        {
        }

        public SerializableType(System.Type type)
        {
            Type = type;
        }

        public static implicit operator System.Type(SerializableType type)
        {
            return type.Type;
        }

        public static implicit operator SerializableType(System.Type type)
        {
            return new SerializableType(type);
        }

        /// <summary>
        /// Gets the System.Type
        /// </summary>
        public System.Type Type
        {
            get
            {
                if (m_type == null)
                {
                    if (string.IsNullOrEmpty(m_typeName))
                    {
                        throw new TypeUnsetException("Cannot get the type of a SerializableType if the type is unset.");
                    }
                    m_type = System.Type.GetType(m_typeName);
                }
                return m_type;
            }
            set
            {
                m_type = value;
                m_typeName = value.FullName;
            }
        }

        public class TypeUnsetException : System.Exception
        {
            public TypeUnsetException()
            {
            }

            public TypeUnsetException(string message)
                : base(message)
            {
            }
        }

        [SerializeField]
        private string m_typeName;

        private System.Type m_type = null;
    }
}
