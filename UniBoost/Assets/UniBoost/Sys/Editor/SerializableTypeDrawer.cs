//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;

namespace UniBoost.Sys
{
    [CustomPropertyDrawer(typeof(SerializableType))]
    public class SerializableTypeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty typeProperty = property.FindPropertyRelative("m_typeName");

            if (attribute != null)
            {
                LimitToTypeList typeLimiter = attribute as LimitToTypeList;
                LimitToAttribute attrLimiter = attribute as LimitToAttribute;
                if (typeLimiter != null)
                {
                    string[] typeNames = (from type in typeLimiter.ValidTypes
                                          select type.FullName).ToArray();
                    int index = System.Array.IndexOf(typeNames, typeProperty.stringValue);
                    int newIndex = EditorGUI.Popup(position, label.text, index, typeNames);
                    if (index != newIndex)
                    {
                        typeProperty.stringValue = typeLimiter.ValidTypes[newIndex].FullName;
                    }
                }
                else if (attrLimiter != null)
                {
                    string[] typeNames = (from type in attrLimiter.ValidTypes
                                          select type.FullName).ToArray();
                    int index = System.Array.IndexOf(typeNames, typeProperty.stringValue);
                    int newIndex = EditorGUI.Popup(position, label.text, index, typeNames);
                    if (index != newIndex)
                    {
                        typeProperty.stringValue = attrLimiter.ValidTypes[newIndex].FullName;
                    }
                }
                else
                {
                    EditorGUI.PropertyField(position, typeProperty, label);
                }
            }
            else
            {
                EditorGUI.PropertyField(position, typeProperty, label);
            }
        }

        //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        //{
        //    return base.GetPropertyHeight(property, label);
        //}
    }
}
