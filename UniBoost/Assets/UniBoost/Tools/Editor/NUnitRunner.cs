using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;
using System.Linq;
using NUnit.Framework.Api;
using NUnit.Framework.Internal;
using System.IO;
using System.Collections.Generic;

namespace UniBoost.Tools
{
    public class NUnitRunner : EditorWindow, ITestListener
    {
        private static string s_windowTitle = "UB NUnit Runner";

        [MenuItem("Window/UniBoost/NUnit Output")]
        public static void ShowNUintOutputWindow()
        {
            GetWindow<NUnitRunner>(s_windowTitle);
        }


        [MenuItem("File/Run Unit Tests %u")]
        public static void RunNUnitTests()
        {
            NUnitRunner runner = GetWindow<NUnitRunner>(s_windowTitle);

            runner.RunTests();
        }

        public void RunTests()
        {
            // To get the data out of NUnit in a more useful manner, we're effectively writing our own
            // runner.
            Assembly assembly = (from assem in System.AppDomain.CurrentDomain.GetAssemblies()
                                 where assem.GetName().Name == "Assembly-CSharp"
                                 select assem).First();

            // Create a runner
            ITestAssemblyRunner runner = new NUnitLiteTestAssemblyRunner(new NUnitLiteTestAssemblyBuilder());

            // Load the assembly
            runner.Load(assembly, new Hashtable()); // we're not passing any options through.

            // run the tests
            m_result = runner.Run(this, TestFilter.Empty);


        }

        public void TestStarted(ITest test)
        {
        }

        public void TestFinished(ITestResult result)
        {
        }

        public void TestOutput(TestOutput output)
        {
        }

        void DisplayTestResult(ITestResult result, string path)
        {

            if (result.HasChildren)
            {
                foreach (ITestResult childResult in result.Children)
                {
                    DisplayTestResult(childResult, path + "/" + result.Name);
                }
            }
            else if (result.ResultState != ResultState.Success)
            {
                string info = string.Format("{0}/{1} {2}", path, result.Name, result.ResultState.ToString());
                info += result.Message + "\n";
                info += result.StackTrace;
                EditorGUILayout.HelpBox(info, result.ResultState == ResultState.Success ? MessageType.Info : MessageType.Warning);

                if ((Event.current.type == EventType.MouseDown) &&
                    (Event.current.clickCount > 0) &&
                    GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition) &&
                    (Event.current.clickCount == 2) )
                {
                    OpenFile(result.StackTrace);
                }
            }
        }

        private static void OpenFile(string stackTrace)
        {

            Assembly assembly = Assembly.GetAssembly(typeof(UnityEditor.SceneView));
            System.Type type = assembly.GetType("UnityEditorInternal.InternalEditorUtility");
            if (type == null)
            {
                Debug.Log("Failed to open source file");
                return;
            }

            string[] lines = stackTrace.Split('\n');
            int inIndex = lines[0].IndexOf(" in ") + 4;
            string filename = lines[0].Substring(inIndex);

            int colonPos = filename.LastIndexOf(':');
            int lineNumber = System.Int32.Parse(filename.Substring(colonPos + 1));
            filename = filename.Substring(0, colonPos);

            //Debug.Log(string.Format("\"{0}\" : '{1}'", filename, lineNumber));

            MethodInfo method = type.GetMethod("OpenFileAtLineExternal");
            method.Invoke(method, new object[] { filename, lineNumber });
        }



        void OnGUI()
        {

            if (GUILayout.Button("Run Unit Tests"))
            {
                RunTests();
            }

            if (m_result == null)
            {
                EditorGUILayout.HelpBox("No Unit tests run yet.", MessageType.Info);
                return;
            }
            EditorGUILayout.BeginScrollView(m_scrollPos);

            if (m_result.ResultState == ResultState.Success)
            {
                string info = string.Format("Success! {0} Test{1} passed.", m_result.PassCount, m_result.PassCount == 1 ? "" : "s");
                EditorGUILayout.HelpBox(info, MessageType.Info);
            }
            else
            {
                DisplayTestResult(m_result, "");
            }

            EditorGUILayout.EndScrollView();
        }


        private ITestResult m_result = null;
        Vector2 m_scrollPos = Vector2.zero;

    }
}
