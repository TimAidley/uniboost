using UnityEngine;
using System.Collections;
using UniBoost.Sys;

namespace UniBoost.Events
{
    [System.Serializable]
    public abstract class EventReaction
    {
        public SerializableEnum Trigger;

        public abstract void React(GameObject origin, params object[] parameters);

    }
}
