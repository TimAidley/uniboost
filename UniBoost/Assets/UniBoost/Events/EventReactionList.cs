using UnityEngine;
using System.Collections.Generic;

using UniBoost.Sys;

namespace UniBoost.Events
{
    public interface IEventReactionList
    {
        IEnumerable<EventReaction> Reactions
        {
            get;
        }

    }
}