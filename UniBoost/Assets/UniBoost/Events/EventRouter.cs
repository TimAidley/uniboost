//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using System.Collections.Generic;

namespace UniBoost.Events
{
    public class EventRouter
    {
        public static EventRouter Global = new EventRouter();  // Worth having a global instance? not sure!

        public delegate void EventCallback(System.Enum eventId, params object[] parameters);

        public void Subscribe(EventCallback callback)
        {
            m_anyTypeCallback += callback;
        }

        public void Subscribe(EventCallback callback, System.Type idType)
        {
            EventCallback foundCallback;
            if (m_typeCallbacks.TryGetValue(idType, out foundCallback))
            {
                foundCallback += callback;
                m_typeCallbacks[idType] = foundCallback;
            }
            else
            {
                m_typeCallbacks[idType] = callback;
            }
        }

        public void Subscribe(EventCallback callback, params System.Enum[] enumTriggers)
        {
            EventCallback foundCallback;
            foreach (System.Enum trigger in enumTriggers)
            {
                if (m_enumCallbacks.TryGetValue(trigger, out foundCallback))
                {
                    foundCallback += callback;
                    m_enumCallbacks[trigger] = foundCallback;
                }
                else
                {
                    m_enumCallbacks[trigger] = callback;
                }
            }
        }

        public void Publish(System.Enum eventId, params object[] parameters)
        {
            EventCallback foundCallback;
            if (m_enumCallbacks.TryGetValue(eventId, out foundCallback))
            {
                foundCallback(eventId, parameters);
            }
            if (m_typeCallbacks.TryGetValue(eventId.GetType(), out foundCallback))
            {
                foundCallback(eventId, parameters);
            }
            m_anyTypeCallback(eventId, parameters);
        }

        public void Unsubscribe(EventCallback callback)
        {
            m_anyTypeCallback -= callback;
        }

        public void Unsubscribe(EventCallback callback, System.Type idType)
        {
            EventCallback foundCallback;
            if (m_typeCallbacks.TryGetValue(idType, out foundCallback))
            {
                foundCallback -= callback;
                m_typeCallbacks[idType] = foundCallback;
            }
        }

        public void Unsubscribe(EventCallback callback, params System.Enum[] enumTriggers )
        {
            EventCallback foundCallback;
            foreach (System.Enum trigger in enumTriggers)
            {
                if (m_enumCallbacks.TryGetValue(trigger, out foundCallback))
                {
                    foundCallback -= callback;
                    m_enumCallbacks[trigger] = foundCallback;
                }
            }
        }


        private EventCallback m_anyTypeCallback = delegate { };

        private Dictionary<System.Type, EventCallback> m_typeCallbacks = new Dictionary<System.Type, EventCallback>();

        private Dictionary<System.Enum, EventCallback> m_enumCallbacks = new Dictionary<System.Enum, EventCallback>();

        


        /// <summary>
        /// A disposable class to handle unsubscription automatically
        /// </summary>
        /// <example>
        /// using (new EventRouter.Subscription(MyEnum.Value, MyCallback))
        /// {
        /// 	// do something
        /// }
        /// </example>
        public class Subscription : System.IDisposable
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="UniBoost.Events.EventRouter.Subscription"/> class.
            /// This version intercepts all events.
            /// </summary>
            /// <param name='router'>
            /// The event router the subscription is with.
            /// </param>
            /// <param name='callback'>
            /// The delegate to call when an event comes in.
            /// </param>
            public Subscription(EventRouter router, EventCallback callback)
            {
                router.Subscribe(callback);
                m_callback = callback;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="UniBoost.Events.EventRouter.Subscription"/> class.
            /// This version intercepts only events of a given type.
            /// </summary>
            /// <param name='router'>
            /// The event router the subscription is with.
            /// </param>
            /// <param name='idType'>
            /// The type 
            /// </param>
            /// <param name='callback'>
            /// The delegate to call when an event comes in.
            /// </param>
            public Subscription(EventRouter router, System.Type idType, EventCallback callback)
            {
                router.Subscribe(callback, idType);
                m_subscriptionType = idType;
                m_callback = callback;
            }

            ~Subscription()
            {
                Dispose();
            }

            public void Dispose()
            {
                if (m_router != null)
                {
                    if (m_subscriptionType == null)
                    {
                        m_router.Unsubscribe(m_callback);
                    }
                    else
                    {
                        m_router.Unsubscribe(m_callback, m_subscriptionType);
                    }
                }
            }


            private System.Type m_subscriptionType = null;
            private EventRouter m_router = null;
            private EventCallback m_callback = null;
        }

    }
}
