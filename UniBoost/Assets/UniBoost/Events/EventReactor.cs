//          Copyright Timothy Aidley 2013
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//         http://opensource.org/licenses/BSL-1.0)

using UnityEngine;
using System.Collections;
using UniBoost.Sys;
using System.Linq;

namespace UniBoost.Events
{
    public class EventReactor : MonoBehaviour
    {
        [LimitToTypeList(LimitToTypeListOptions.SpecifiedOnly, typeof(GameEventEnumAttribute))]
        public SerializableType TriggerType;

        public void OnEnable()
        {
            m_reactionLists = (from component in GetComponents<Component>()
                               let iterf = component as IEventReactionList
                               where iterf != null
                               select iterf).ToArray();

            EventRouter.Global.Subscribe(TriggerType, React);
        }

        public void OnDisable()
        {
            EventRouter.Global.Unsubscribe(TriggerType, React);
        }


        void React(System.Enum eventValue, GameObject origin, params object[] parameters)
        {
            // TODO : Use dictionary
            foreach (IEventReactionList reactionList in m_reactionLists)
            {
                foreach (EventReaction reaction in reactionList.Reactions)
                {
                    if (eventValue.Equals(reaction.Trigger))
                    {
                        reaction.React(origin, parameters);
                    }
                }
            }
        }

        private IEventReactionList[] m_reactionLists;
    }
}
